package delivery

import (
	"net/http"
	docs "profile/internal/delivery/http/swagger/docs"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-gonic/gin"
)

// @title user profiles service
// @version 1.0
// @description user profiles service
// @license.name kanya384

// @contact.name API Support
// @contact.email kanya384@mail.ru

// @BasePath /
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func (d *Delivery) initRouter() *gin.Engine {

	var router = gin.New()
	d.routerDocs(router.Group("/docs"))
	router.POST("/sign-up", d.CreateUser)
	router.POST("/user/creds", d.ReadUserByCredetinals)
	router.Use(d.checkAuth)
	d.routerUser(router.Group("/user"))

	return router
}

func (d *Delivery) routerUser(router *gin.RouterGroup) {
	router.GET("/:id", d.ReadUserById)
	router.PUT("/:id", d.UpdateUser)
	router.DELETE("/:id", d.DeleteUserById)
}

func (d *Delivery) routerDocs(router *gin.RouterGroup) {

	docs.SwaggerInfo.BasePath = "/"

	router.Any("/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}

func (d *Delivery) checkAuth(c *gin.Context) {
	token := c.GetHeader("x-auth-token")
	if token == "" {
		c.AbortWithStatusJSON(http.StatusUnauthorized, "token not provided")
		return
	}

	result, err := d.tm.Parse(token)
	if err != nil {
		d.logger.Error("error parsing token - %s: %s", token, err.Error())
		c.AbortWithStatusJSON(http.StatusUnauthorized, "token parse error")
		return
	}

	setAuthInfoToContext(c, result.UserID, result.Login)

	c.Next()
}

func setAuthInfoToContext(c *gin.Context, userId string, login string) {
	c.Set("userId", userId)
	c.Set("userName", login)
}
