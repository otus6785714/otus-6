package delivery

import (
	docs "auth/internal/delivery/http/swagger/docs"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-gonic/gin"
)

// @title leadactiv backend service
// @version 1.0
// @description leadactiv backend service
// @license.name kanya384

// @contact.name API Support
// @contact.email kanya384@mail.ru

// @BasePath /
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func (d *Delivery) initRouter() *gin.Engine {

	var router = gin.New()
	router.LoadHTMLFiles("./internal/templates/sign-in.html", "./internal/templates/sign-up.html")
	router.Static("/.well-known", ".well-known")
	d.routerDocs(router.Group("/docs"))

	router.POST("/sign-in", d.SignIn)
	router.GET("/sign-out", d.SignOut)
	router.GET("/session/:id", d.ReadSessionById)
	router.GET("/session/cookie", d.ReadSessionByCookie)

	return router
}

func (d *Delivery) routerDocs(router *gin.RouterGroup) {

	docs.SwaggerInfo.BasePath = "/"

	router.Any("/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
