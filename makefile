minikube-start:
	@minikube start --driver virtualbox --cpus=4 --memory=16g --cni=flannel --kubernetes-version="v1.19.0"

helm-install:
	@helm install app ./chart

run-newman:
	@newman run ./collection.postman.json
helm-remove:
	@helm uninstall app
